### What is this repository for? ###

This repository is for the code of my diploma thesis on sentiment analysis over a large twitter dataset using n-gram graphs.

### How do I get set up? ###

After cloning the repo to a folder you can import the project using your favorite Java IDE(i.e Eclipse,Netbeans etc.).
Make sure the two weka jar file's are included in the build path and the ProjectConstants.java is configured correctly.


### Who do I talk to? ###

Michalis Ellinas 
e-mail: ellinasm91@hotmail.com