package myPackage;

import java.util.Map.Entry;

public class GraphMerge {
	// Here we will merge NGramHash edges to one big hashtable
	// In this hashtable each element will be an Ngram edge.
	public NgramHash hashMerge(NgramHash bigOne, NgramHash smallOne) {
		// We will merge all edges from the small table to the big one. If an
		// edge
		// already exists we add the two frequencies.
		for (Entry<String, Integer> smallentry : smallOne.entrySet()) {
			String tempkey = smallentry.getKey();
			if (bigOne.containsKey(tempkey)) {
				Integer temp = bigOne.get(tempkey);
				bigOne.put(tempkey,temp + smallentry.getValue());
			} else {

				bigOne.put(tempkey, smallentry.getValue());
			}
		}
		return bigOne;

	}
}
