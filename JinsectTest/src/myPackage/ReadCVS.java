package myPackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

public class ReadCVS {

	String csvFile = ProjectConstants.myPath + "/training.1600000.processed.noemoticon.csv";

	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = "\",\"";
	Long lineNo;
	HashSet<Tweet> myTable = new HashSet<Tweet>();
	Iterator<Tweet> myIterator;

	public Tweet getnextTweet() {
		if (myIterator.hasNext()) {
			return myIterator.next();
		}
		return null;
	}

	public ReadCVS() {
		try {
			lineNo=0L;
			br = new BufferedReader(new FileReader(csvFile));
			if (lineNo < (800000-(ProjectConstants.dataSize/2))){
			while (((line = br.readLine()) != null)&& (lineNo < (800000-(ProjectConstants.dataSize/2)))) {
				lineNo++;
			}}
			lineNo=0L;
			
			String[] country;
			while (((line = br.readLine()) != null)&& (lineNo<ProjectConstants.dataSize)) {
				// use comma as separator
				country = line.split(cvsSplitBy);
				String sent=quoteCleaner(country[5]);
				
				String twe=quoteCleaner(country[0]);
				
				//System.out.println(sent +"   "+twe);
				myTable.add(new Tweet(sent, twe));
				lineNo++;
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		myIterator = myTable.iterator();
		//System.out.println("CvsReader lineNO: "+lineNo);
	}
	public String quoteCleaner(String a){
		String temp=a;
		if (temp.charAt(temp.length()-1)=='\"'){
			temp=temp.substring(0,temp.length()-1);
			
		}
		if (temp.charAt(0)=='\"'){
			temp=temp.substring(1,temp.length());
			
		}
		return temp;
	}

}