package myPackage;

public class Tweet {
	private String myTweet;
	private String oppinion;
	public Tweet(String myTweet, String oppinion) {
		this.myTweet = preProcessTweet(myTweet);
		this.oppinion = oppinion;
		//System.out.println(myTweet+"---->"+oppinion);
	}
	public  String preProcessTweet(String tweet){
		String temp;
		//Convert to lower case
		temp = tweet.toLowerCase();
	    //Convert www.* or https?://* to URL
		String regex = "(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		temp=temp.replaceAll(regex, "URL");

	    //Convert @username to AT_USER
		temp = temp.replaceAll("@[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]","AT_USER");
	    //Remove additional white spaces
		temp = temp.replaceAll("\\s\\s", " ");
	    //Replace #word with word
		temp = temp.replaceAll("#(?i)(-a-zA-Z0-9+&@#/%=~_|)", "$1");
	    return temp;
		
	}
	public String getTweet() {
		return myTweet;
	}
	public void setTweet(String myTweet) {
		this.myTweet = myTweet;
	}
	public String getOppinion() {
		return oppinion;
	}
	public void setOppinion(String oppinion) {
		this.oppinion = oppinion;
	}
	
}
