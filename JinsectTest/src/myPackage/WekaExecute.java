package myPackage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.*;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;

public class WekaExecute {

	public static void main(String[] args) throws IOException {
		PrintWriter writer = new PrintWriter(ProjectConstants.myPath + "/test3-output.txt", "UTF-8");

		// TODO Auto-generated method stub
		SystemInfo sysinfo = new SystemInfo();
		BufferedReader reader;
		Instances data; // contains the full dataset we wann create train/test
						// sets from
		int seed = 4; // the seed for randomizing the data
		int folds = 2; // the number of folds to generate, >=2
		/*
		String[] filenames = { "gram-NVS.arff", "gram-Cos.arff", "gram-Eu.arff", "gram-CS.arff", "gram-VS.arff",
				"gram-CosEU.arff", "gram-CosCon.arff", "gram-CosNVS.arff", "gram-ContNVS.arff", "gram-VSNVS.arff" };
		*/
		String[] filenames ={"gram-NVS2.arff","gram-VS2.arff","gram-Cont2.arff",
				"gram-Eu2.arff","gram-Cos2.arff"};
		for (String filename : filenames) {
			writer.println();
			writer.println(filename);
			writer.println();

			reader = new BufferedReader(new FileReader(ProjectConstants.myPath + "/test2-"
					+ (ProjectConstants.dataSize / 1000) + "-" + ProjectConstants.nGramSize + filename));
			ArffReader arff = new ArffReader(reader);
			data = arff.getData();
			data.setClassIndex(data.numAttributes() - 1);
			Instances ranData = new Instances(data);
			Random rand = new Random(seed);
			ranData.randomize(rand);
			double[] corClassified = new double[10];

			Classifier[] cModelarray = { (Classifier) new NaiveBayes(), (Classifier) new Logistic(),
					(Classifier) new SMO(), (Classifier) new J48() };
			for (Classifier cModel : cModelarray) {
				double corClassSum = 0;
				long startTime = System.currentTimeMillis();
				ranData.randomize(rand);
				for (int n = 0; n < 2; n++) {

					Instances train = ranData.trainCV(folds, n);
					Instances test = ranData.testCV(folds, n);

					// Classifier cModel = (Classifier) new NaiveBayes();
					Evaluation eTest = null;
					// further processing, classification, etc.
					try {

						cModel.buildClassifier(train);
						eTest = new Evaluation(test);
						eTest.evaluateModel(cModel, test);
						corClassified[n] = eTest.pctCorrect();
						corClassSum += corClassified[n];
						// writer.println("ERROR RATE: "+eTest.errorRate());
						// writer.println("Correctly Classified:
						// "+eTest.pctCorrect());
						// writer.println(strSummary);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace(writer);
					}
					/*
					 * // Get the confusion matrix double[][] cmMatrix =
					 * eTest.confusionMatrix(); for (int i = 0; i <
					 * cmMatrix.length; i++) { for (int j = 0; j <
					 * cmMatrix[i].length; j++) {
					 * writer.print(cmMatrix[i][j] + " "); }
					 * writer.println(""); } writer.println(cmMatrix);
					 */
				}
				writer.println(cModel.getClass());
				NumberFormat formatter = new DecimalFormat("#0.00");

				writer.println("Average ratio of correctly classified: " + formatter.format(corClassSum / 2) + "%");
				long goldTime = System.currentTimeMillis();
				writer.println("Building the golden roules took " + (goldTime - startTime) / 1000 + " s");
				writer.println(sysinfo.MemInfo());
			}

		}
		writer.close();
	}
}