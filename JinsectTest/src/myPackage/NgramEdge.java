package myPackage;

public class NgramEdge {
	Ngram a;
	Ngram b;
	Integer freq;

	public double getFreq() {
		return freq;
	}

	public void setFreq(Integer d) {
		this.freq = d;
	}

	public NgramEdge(Ngram a, Ngram b, int freq) {
		this.a = a;
		this.b = b;
		this.freq = freq;

	}

	public boolean equals(NgramEdge that) {
		if ((this.a.equals(that.a))&&(this.b.equals(that.b))
				&&(this.freq==(that.freq))){
			return true;
		}
		else return false;
	}

	public String myhashCode() {
		// TODO Auto-generated method stub
		return (a.getMyName()+"-->"+b.getMyName());
	}
	

}
