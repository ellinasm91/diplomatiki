package myPackage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map.Entry;

public class HashCSVWrite {
	public HashCSVWrite(NgramHash p, NgramHash n,String csvFileP, String csvFileN) {
		generateCsvFile(p, csvFileP);
		generateCsvFile(n, csvFileN);
	}

	private static void generateCsvFile(NgramHash ngh, String sFileName) {
		try {
			FileWriter writer = new FileWriter(sFileName);
			writer.append("nGramName");
			writer.append(',');
			writer.append("Weight");
			writer.append('\n');
			for (Entry<String, Integer> entry : ngh.entrySet()) {
				writer.append("\""+entry.getKey()+"\"");
				writer.append(',');
				writer.append("\""+entry.getValue().toString()+"\"");
				writer.append('\n');
			}

			// generate whatever data you want

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
