package myPackage;

public class GoldenHashTable {

	private NgramHash positiveGold, negativeGold;
	ReadCVS reader = null;
	public NgramHash getPositiveGold() {
		return positiveGold;
	}

	public NgramHash getNegativeGold() {
		return negativeGold;
	}

	public GoldenHashTable(ReadCVS reader2) {
		this.reader = reader2;


		// We use the parser to get the tweets in
		// the form of an iterator.
		Tweet temp = reader.getnextTweet();
		int i = 0;
		//positiveGold=new NgramHash();
		//negativeGold=new NgramHash();
		// Then we create the NgramHash for each individual tweet and merge to
		// the golden NgramHash tables accordingly.
		positiveGold = new NgramHash("", ProjectConstants.nGramSize);
		negativeGold = new NgramHash("", ProjectConstants.nGramSize);
		if (temp.getOppinion().equals("0")) {
			negativeGold = new NgramHash(temp.getTweet(), ProjectConstants.nGramSize);
		} else if (temp.getOppinion().equals("4")) {
			positiveGold = new NgramHash(temp.getTweet(), ProjectConstants.nGramSize);
		} else {
			System.out.println("Error in oppinion parsing. Value= " + temp.getOppinion());
		}

		while (((temp = reader.getnextTweet()) != null)){ 
				//&& (i < 10000)) {
			if (temp.getOppinion().equals("0")) {
				negativeGold = negativeGold.hashMerge(new NgramHash(temp.getTweet(), ProjectConstants.nGramSize));
				
			} else if (temp.getOppinion().equals("4")) {
				positiveGold = positiveGold.hashMerge(new NgramHash(temp.getTweet(), ProjectConstants.nGramSize));
			} else {
				System.out.println("Error in oppinion parsing. Value= " + temp.getOppinion());
			}

			i++;

		}
		System.out.println("Number of tweets prosecced during Golden creation= "+i);

	}
	
	

	

}
