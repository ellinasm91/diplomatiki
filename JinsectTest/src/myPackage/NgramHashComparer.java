package myPackage;

import java.util.Map.Entry;

public class NgramHashComparer {
	public NgramHashComparer() {
	}

	@SuppressWarnings("unused")
	public wekaData myComparer(NgramHash goldenP, NgramHash goldenN, ReadCVS reader) {
		wekaData wekadata = new wekaData(1);
		Tweet temp;

		// We create two sets of vectors one for comparing with the negative and
		// one with the positive golden sets.
		Integer[] bigArrayP = new Integer[10000];
		Integer[] bigArrayN = new Integer[10000];
		Integer[] smallArrayP = new Integer[10000];
		Integer[] smallArrayN = new Integer[10000];
		int counter = 0;
		while (((temp = reader.getnextTweet()) != null)) {
			// for (int p = 0; p < 10000; p++) {
			int i = 0, j = 0;
			counter++;
			// if ((temp = reader.getnextTweet()) != null) {

			NgramHash newnh = new NgramHash(temp.getTweet(), ProjectConstants.nGramSize);
			double valueRatioP = 0, valueRatioN = 0;
			for (Entry<String, Integer> entry : newnh.entrySet()) {
				String tempkey = entry.getKey();
				if (goldenP.containsKey(tempkey)) {
					bigArrayP[i] = goldenP.get(tempkey);
					smallArrayP[i] = entry.getValue();
					 valueRatioP += min(bigArrayP[i], smallArrayP[i]) / max(bigArrayP[i], smallArrayP[i]);
					i++;
				}
				if (goldenN.containsKey(tempkey)) {
					bigArrayN[j] = goldenN.get(tempkey);
					smallArrayN[j] = entry.getValue();
					 valueRatioN += min(bigArrayN[j], smallArrayN[j]) /max(bigArrayN[j], smallArrayN[j]);
					j++;
				}
			}

			double containmentSimilarityP = 0, containmentSimilarityN = 0, valueSimilarityP = 0, valueSimilarityN = 0,
					normalizedValueSimilarityP = 0, normalizedValueSimilarityN = 0;
			
			if ((goldenP.size() == 0) || (goldenN.size() == 0) || newnh.size() == 0) {
				System.out.println(goldenP.size() + ", " + goldenN.size() + ", " + newnh.size());
			} else {
				// Containment similarity is the proportion of edges that are
				// shared

				//containmentSimilarityP = i / min(goldenP.size(), newnh.size());
				//containmentSimilarityN = j / min(goldenN.size(),newnh.size());

				// Value Similarity considers the weights.
				 //valueSimilarityP = valueRatioP / max(newnh.size(), goldenP.size());
				// valueSimilarityN = valueRatioN / max(newnh.size(), goldenN.size());

				// Normalized VS decouples from the largest graph's size.
				// NVS=VS/(min()/max()) or :
				 normalizedValueSimilarityP = valueRatioP / min(newnh.size(), goldenP.size());
				 normalizedValueSimilarityN = valueRatioN / min(newnh.size(), goldenN.size());
			}
			 
			//double CSP = cosineSimilarity(bigArrayP, smallArrayP, i);
			//double eP = euclideanDistance(bigArrayP, smallArrayP,i);
			//double CSN = cosineSimilarity(bigArrayN, smallArrayN, j);
			//double eN = euclideanDistance(bigArrayN, smallArrayN,j);
			
			//Depending on the number of features we choose the appropriate variation of addInstance
			/*
			 * wekadata.addInstance(containmentSimilarityP,
			 * containmentSimilarityN, valueSimilarityP, valueSimilarityN,
			 * normalizedValueSimilarityP, normalizedValueSimilarityN,
			 * toSentiment(temp.getOppinion()));
			 */
			wekadata.addInstance(normalizedValueSimilarityP,normalizedValueSimilarityN,toSentiment(temp.getOppinion()));
		}
		System.out.println("Number of tweet read during comparing= " + counter);
		return wekadata;

	}

	private double min(Integer bigArrayP, Integer smallArrayP) {
		// TODO Auto-generated method stub
		if (bigArrayP > smallArrayP) {
			return smallArrayP;
		} else
			return bigArrayP;
	}

	private double max(Integer bigArrayP, Integer smallArrayP) {
		// TODO Auto-generated method stub
		if (bigArrayP < smallArrayP) {
			return smallArrayP;
		} else
			return bigArrayP;
	}

	public static double cosineSimilarity(Integer[] bigArrayP, Integer[] smallArrayP, int length) {
		double dotProduct = 0.0;
		double normA = 0.0;
		double normB = 0.0;
		for (int j = 0; j < length; j++) {
			dotProduct += bigArrayP[j] * smallArrayP[j];
			normA += Math.pow(bigArrayP[j], 2);
			normB += Math.pow(smallArrayP[j], 2);
		}
		return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}

	public double euclideanDistance(Integer[] bigArrayP, Integer[] smallArrayP,int alength) {
		double Sum = 0.0;
		for (int i = 0; i < alength; i++) {
			Sum = Sum + (bigArrayP[i] - smallArrayP[i])*(bigArrayP[i] - smallArrayP[i]);
		}
		return Math.sqrt(Sum);
	}

	public String toSentiment(String a) {
		if (a.equals("0"))
			return ("negative");
		else if (a.equals("4"))
			return ("positive");
		else
			return null;

	}

}
