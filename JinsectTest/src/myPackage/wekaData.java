package myPackage;

import java.util.ArrayList;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class wekaData{
	Instances isTrainingSet, isTestingSet, isAllSet;
	ArrayList<Attribute> fvWekaAttributes;
	// Create a na�ve bayes classifier
	Classifier cModel = (Classifier) new BayesNet();
	Evaluation eTest = null;
	int folds = 10;

	public wekaData   (int numOfFeatures) {
		if (numOfFeatures == 1) {
			// Declare two numeric attributes
			Attribute Attribute1 = new Attribute("CosinePos");
			Attribute Attribute2 = new Attribute("CosineNeg");

			// Declare the class attribute along with its values
			ArrayList<String> fvClassVal = new ArrayList<String>();
			fvClassVal.add("positive");
			fvClassVal.add("negative");
			Attribute ClassAttribute = new Attribute("theClass", fvClassVal);

			// Declare the feature vector
			fvWekaAttributes = new ArrayList<Attribute>();
			fvWekaAttributes.add(Attribute1);
			fvWekaAttributes.add(Attribute2);
			fvWekaAttributes.add(ClassAttribute);

			// Create an empty training set
			// (name, attributes list, size)
			this.isAllSet = new Instances("Rel", fvWekaAttributes, 2);
			this.isTrainingSet = new Instances("Rel", fvWekaAttributes, 2);
			this.isTestingSet = new Instances("Rel", fvWekaAttributes, 2);
			// Set class index
			this.isTrainingSet.setClassIndex(2);
			this.isTestingSet.setClassIndex(2);
			this.isAllSet.setClassIndex(2);
		} else if (numOfFeatures == 2) {
			// Declare two numeric attributes
			Attribute Attribute1 = new Attribute("CosinePos");
			Attribute Attribute2 = new Attribute("CosineNeg");
			Attribute Attribute3 = new Attribute("EuclideanPos");
			Attribute Attribute4 = new Attribute("EuclideanNeg");
			// Declare the class attribute along with its values
			ArrayList<String> fvClassVal = new ArrayList<String>();
			fvClassVal.add("positive");
			fvClassVal.add("negative");
			Attribute ClassAttribute = new Attribute("theClass", fvClassVal);

			// Declare the feature vector
			fvWekaAttributes = new ArrayList<Attribute>();
			fvWekaAttributes.add(Attribute1);
			fvWekaAttributes.add(Attribute2);
			fvWekaAttributes.add(Attribute3);
			fvWekaAttributes.add(Attribute4);
			fvWekaAttributes.add(ClassAttribute);

			// Create an empty training set
			// (name, attributes list, size)
			this.isAllSet = new Instances("Rel", fvWekaAttributes, 4);
			this.isTrainingSet = new Instances("Rel", fvWekaAttributes, 4);
			this.isTestingSet = new Instances("Rel", fvWekaAttributes, 4);
			// Set class index
			this.isTrainingSet.setClassIndex(4);
			this.isTestingSet.setClassIndex(4);
			this.isAllSet.setClassIndex(4);
		} else if (numOfFeatures == 3) {
			// Declare two numeric attributes
			Attribute Attribute1 = new Attribute("1Pos");
			Attribute Attribute2 = new Attribute("1Neg");
			Attribute Attribute3 = new Attribute("2Pos");
			Attribute Attribute4 = new Attribute("2Neg");
			Attribute Attribute5 = new Attribute("3Pos");
			Attribute Attribute6 = new Attribute("3Neg");
			// Declare the class attribute along with its values
			ArrayList<String> fvClassVal = new ArrayList<String>();
			fvClassVal.add("positive");
			fvClassVal.add("negative");
			Attribute ClassAttribute = new Attribute("theClass", fvClassVal);

			// Declare the feature vector
			fvWekaAttributes = new ArrayList<Attribute>();
			fvWekaAttributes.add(Attribute1);
			fvWekaAttributes.add(Attribute2);
			fvWekaAttributes.add(Attribute3);
			fvWekaAttributes.add(Attribute4);
			fvWekaAttributes.add(Attribute5);
			fvWekaAttributes.add(Attribute6);
			fvWekaAttributes.add(ClassAttribute);

			// Create an empty training set
			// (name, attributes list, size)
			this.isAllSet = new Instances("Rel", fvWekaAttributes, 6);
			this.isTrainingSet = new Instances("Rel", fvWekaAttributes, 6);
			this.isTestingSet = new Instances("Rel", fvWekaAttributes, 6);
			// Set class index
			this.isTrainingSet.setClassIndex(6);
			this.isTestingSet.setClassIndex(6);
			this.isAllSet.setClassIndex(6);
		} else {
			System.out.println("Please use 1-3 types of features");
		}
	}

	public void addInstance(double posSimilarity, double negSimilarity, String sentiment) {
		Instance iExample = new DenseInstance(4);
		iExample.setValue((Attribute) fvWekaAttributes.get(0), posSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(1), negSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(2), sentiment);

		this.isAllSet.add(iExample);
	}

	public void addInstance(double posSimilarity, double negSimilarity, double posEu, double negEu, String sentiment) {
		Instance iExample = new DenseInstance(6);
		iExample.setValue((Attribute) fvWekaAttributes.get(0), posSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(1), negSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(2), posEu);
		iExample.setValue((Attribute) fvWekaAttributes.get(3), negEu);
		iExample.setValue((Attribute) fvWekaAttributes.get(4), sentiment);

		this.isAllSet.add(iExample);
	}
	public void addInstance(double Pos1, double Neg1,double Pos2, double Neg2, double Pos3, double Neg3, String sentiment) {
		Instance iExample = new DenseInstance(8);
		iExample.setValue((Attribute) fvWekaAttributes.get(0), Pos1);
		iExample.setValue((Attribute) fvWekaAttributes.get(1), Neg1);
		iExample.setValue((Attribute) fvWekaAttributes.get(2), Pos2);
		iExample.setValue((Attribute) fvWekaAttributes.get(3), Neg2);
		iExample.setValue((Attribute) fvWekaAttributes.get(4), Pos3);
		iExample.setValue((Attribute) fvWekaAttributes.get(5), Neg3);
		iExample.setValue((Attribute) fvWekaAttributes.get(6), sentiment);

		this.isAllSet.add(iExample);
	}
/*
	public void addInstanceTraining(double posSimilarity, double negSimilarity, String sentiment) {
		Instance iExample = new DenseInstance(4);
		iExample.setValue((Attribute) fvWekaAttributes.get(0), posSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(1), negSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(2), sentiment);

		this.isTrainingSet.add(iExample);
	}

	public void addInstanceTesting(double posSimilarity, double negSimilarity, String sentiment) {
		Instance iExample = new DenseInstance(4);
		iExample.setValue((Attribute) fvWekaAttributes.get(0), posSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(1), negSimilarity);
		iExample.setValue((Attribute) fvWekaAttributes.get(2), sentiment);

		this.isTestingSet.add(iExample);
	}

	public void runTheClassifier() {
		for (int n = 0; n < folds; n++) {
			isTrainingSet = isAllSet.trainCV(folds, n);
			isTestingSet = isAllSet.testCV(folds, n);

			// further processing, classification, etc.
			try {
				cModel.buildClassifier(isTrainingSet);
				eTest = new Evaluation(isTrainingSet);
				eTest.evaluateModel(cModel, isTestingSet);
				String strSummary = eTest.toSummaryString();
				System.out.println(strSummary);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace(System.out);
			}

			// Get the confusion matrix
			double[][] cmMatrix = eTest.confusionMatrix();
			for (int i = 0; i < cmMatrix.length; i++) {
				for (int j = 0; j < cmMatrix[i].length; j++) {
					System.out.print(cmMatrix[i][j] + " ");
				}
				System.out.println("");
			}
			System.out.println(cmMatrix);
		}

	}
	*/
}
