package myPackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class HashCSVReader {
	/*
	String csvFileP = ProjectConstants.myPath + "nghPositive" + ProjectConstants.dataSize + "-"
			+ ProjectConstants.nGramSize + "gram.csv";
	String csvFileN = ProjectConstants.myPath + "nghNegative" + ProjectConstants.dataSize + "-"
			+ ProjectConstants.nGramSize + "gram.csv";
	*/
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = "\",\"";
	private NgramHash p, n;

	public NgramHash getP() {
		return p;
	}

	public NgramHash getN() {
		return n;
	}

	public HashCSVReader(String csvFileP,String csvFileN) {
		p = new NgramHash("", ProjectConstants.nGramSize);
		n = new NgramHash("", ProjectConstants.nGramSize);
		HashRead(p, csvFileP);
		HashRead(n, csvFileN);
	}

	public void HashRead(NgramHash ngh, String inp) {
		try {
			
			br = new BufferedReader(new FileReader(inp));
			line=br.readLine();
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] str = line.split(cvsSplitBy);
				ngh.put(quoteCleaner(str[0]), Integer.parseInt(quoteCleaner(str[1])));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public String quoteCleaner(String a){
		String temp=a;
		if (temp.charAt(temp.length()-1)=='\"'){
			temp=temp.substring(0,temp.length()-1);
			
		}
		if (temp.charAt(0)=='\"'){
			temp=temp.substring(1,temp.length());
			
		}
		return temp;
	}
}
