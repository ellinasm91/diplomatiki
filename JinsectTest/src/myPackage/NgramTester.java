package myPackage;

import java.io.File;
import java.io.IOException;

import weka.core.converters.ArffSaver;

public class NgramTester {

	private static long goldTime;

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		SystemInfo sysinfo = new SystemInfo();
		long startTime = System.currentTimeMillis();
		if (ProjectConstants.dataSize > 1600000) {
			System.out.println("Please use an input size smaller than 1600000");
		} else {
			ReadCVS reader;
			NgramHash goldenP, goldenN;
			String csvFileP = ProjectConstants.myPath + "nghPositive" + ProjectConstants.dataSize + "-"
					+ ProjectConstants.nGramSize + "gram.csv";
			String csvFileN = ProjectConstants.myPath + "nghNegative" + ProjectConstants.dataSize + "-"
					+ ProjectConstants.nGramSize + "gram.csv";
			File fileP = new File(csvFileP);
			File fileN = new File(csvFileN);
			if ((fileP.length() == 0) || (fileN.length() == 0)) {
				System.out.println("No file found. Creating the golden roules normally.");
				// This reader with the method getNextTweet returns tweets from
				// the
				// input dataset in the form of an iterator.
				reader = new ReadCVS();
				// Create the two (pos,neg) Golden Roule Tables.
				GoldenHashTable myGHT = new GoldenHashTable(reader);
				goldenP = (myGHT).getPositiveGold(); // goldenP.normalizeNGH();
				goldenN = (myGHT).getNegativeGold();// goldenN.normalizeNGH();
				System.out.println("Success Building merged hash.P= " + goldenP.size() + " N=" + goldenN.size());
				// System.out.println(sysinfo.MemInfo());
				goldTime = System.currentTimeMillis();
				System.out.println("Building the golden roules took " + (goldTime - startTime) / 1000 + " s");
				System.out.println(sysinfo.MemInfo());
				// HashCSVWrite writes to csv file all the edges from the golden
				// rules
				HashCSVWrite hcwrite = new HashCSVWrite(goldenP, goldenN, csvFileP, csvFileN);
			} else {
				System.out.println("File found. Creating the golden roules from file.");
				// HashCSVReader reads from file all the edges for the golden
				// rules
				HashCSVReader hcread = new HashCSVReader(csvFileP, csvFileN);
				goldenP = hcread.getP();
				goldenN = hcread.getN();
				System.out.println(
						"Success reading the golden roules from file.P= " + goldenP.size() + " N=" + goldenN.size());
				goldTime = System.currentTimeMillis();
				System.out.println("Building the golden roules took " + (goldTime - startTime) / 1000 + " s");
				System.out.println(sysinfo.MemInfo());
			}

			goldTime = System.currentTimeMillis();
			//
			// Let's try comparing all the tweets with the merged hashsets.
			// We will use all of the dataset to compare each individual tweet
			// and feed the similarity to our machine learning algorithms.
			reader = new ReadCVS();

			NgramHashComparer a = new NgramHashComparer();
			// NgramHashComparer creates an Instances object
			wekaData wekadata = a.myComparer(goldenP, goldenN, reader);
			System.out.println("Weka Dataset size=" + wekadata.isAllSet.size());
			long endTime = System.currentTimeMillis();
			System.out.println("Comparing took " + (endTime - goldTime) / 1000 + " s");
			System.out.println(sysinfo.MemInfo());
			// ArffSaver is weka's class for writing Instances to arff files
			ArffSaver saver = new ArffSaver();
			saver.setInstances(wekadata.isAllSet);
			try {

				saver.setFile(new File(ProjectConstants.myPath + "/test2-" + (ProjectConstants.dataSize / 1000) + "-"
						+ ProjectConstants.nGramSize + "gram-NVS2.arff"));
				saver.writeBatch();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(System.out);
			}

			// wekadata.runTheClassifier();
			long classifyTime = System.currentTimeMillis();
			System.out.println("Classifier took " + (classifyTime - endTime) / 1000 + " s");
			System.out.println(sysinfo.MemInfo());
			return;
		}
	}

}
