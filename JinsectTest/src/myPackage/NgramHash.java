package myPackage;

import java.util.HashMap;

public class NgramHash extends HashMap<String, Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int n;

	// HashMap<String, NgramEdge> edges;
	/*
	 * public HashMap<String, NgramEdge> getEdges() { return edges; }
	 */
	public NgramHash(String inputString, int nSize) {
		n = nSize;
		// edges = new HashMap<String, NgramEdge>();
		if (inputString.length() >= 1) {
			String[] myNgrams = nGramSplit(inputString);

			for (int i = 0; i < myNgrams.length; i++) {
				// Here we create the edges
				Ngram newNgram = new Ngram(myNgrams[i]);
				// System.out.println(myNgrams[i]);

				for (int j = i; (j < myNgrams.length) && (j < i + n); j++) {
					Ngram secNgram = new Ngram(myNgrams[j]);
					NgramEdge newEdge = new NgramEdge(newNgram, secNgram, 1);
					this.put(newEdge.myhashCode(), newEdge.freq);
				}
			}
		}
	}

	public NgramHash(int i) {
		// TODO Auto-generated constructor stub
		super(i);
	}

	public NgramHash() {
		// TODO Auto-generated constructor stub
		super();
	}

	private String[] nGramSplit(String inputString) {
		int count = 0;
		String[] myArray = new String[inputString.length() - n];
		while (inputString.length() > (count + n)) {

			myArray[count] = inputString.substring(count, count + n);
			count++;
		}
		return myArray;

	}

	////////////////////////////////////////////////////////////////////////
	// We overwrite toString for easy printing.
	String a = "";

	public String toString() {

		for (java.util.Map.Entry<String, Integer> myentry : this.entrySet()) {
			a += (myentry.getKey()) + " " + myentry.getValue();
			a += "\n";
		}

		return a;
	}
	///////////////////////////////////////////////////////////////////////////

	public NgramHash hashMerge(NgramHash smallOne) {
		// We will merge all edges from the small table to this one. If an
		// edge
		// already exists we add the two frequencies.
		for (java.util.Map.Entry<String, Integer> smallentry : smallOne.entrySet()) {
			String tempkey = smallentry.getKey();
			if (this.containsKey(tempkey)) {
				Integer temp = this.get(tempkey) + smallentry.getValue();
				this.put(tempkey, temp);
			} else {

				this.put(tempkey, smallentry.getValue());
			}
		}
		return this;

	}

}
